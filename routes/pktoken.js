var express = require('express');
let pktoken = require('../models/pktoken');
var Usuario = require('../models/usuario');
var Objectid = require('mongodb').ObjectID;

var app = express();

//Obtener pktokens

app.get('/', (req, res, next) => {
    pktoken.find({})
        .populate('duenioActual', 'nombre email')
        .exec(
            (err, pktokens) => {
                if (err) {
                    return res.status(200).json({
                        ok: false,
                        msj: 'Error cargando pktokens',
                        errors: err
                    });
                }
                pktoken.count({}, (err, conteo) => {
                    res.status(200).json({
                        ok: true,
                        usuarios: pktokens,
                        total: conteo
                    });
                });
            });
});

//Pedir un pktoken

app.get('/:id', (req, res, next) => {
    var id = req.params.id;
    pktoken.findById(id, (err, pktoken) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                msj: 'Error al buscar pktoken',
                errors: err
            });
        }

        if (pktoken) {
            return res.status(400).json({
                ok: false,
                msj: 'El pktoken con el id ' + id + ' existe',
                errors: { message: 'genere otro pktoken' }
            });
        }

        return res.status(200).json({
            ok: true,
        });
    });
});

//Crear un pktoken

app.post('/', (req, res) => {

    var body = req.body;
    pktokenData = new pktoken({
        _id: JSON.parse(body._id)._id,
        monto: body.monto,
        fechaCreado: new Date(),
        duenioActual: body.duenioActual,
        estatus: false
    });

    pktokenData.save((err, pktokenGuardado) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                msj: 'Error al buscar el pktoken',
                errors: err
            });
        }

        res.status(201).json({
            ok: true,
            pktokenGuardado
        });
    });
});

//Borrar pktoken

app.delete('/:id' ,( req, res ) => {
    var id = req.params.id;

    pktoken.findByIdAndRemove(id, (err, pktokenBorrado) => {
        if( err ) {
            return res.status(500).json({
                ok: false,
                msj: 'Error al borrar medico',
                errors: err
            });
        }

        if( !pktokenBorrado ) {
            return res.status(500).json({
                ok: false,
                msj: 'No existe medico con ese id',
                errors: { message: 'No existe medico con ese id'}
            });
        }

        res.status(200).json({
            ok: true,
            usuario: pktokenBorrado
        });
    })
});

//Actualizar dueño pktoken

app.put('/:id', (req, res) => {
    var id = req.params.id;
    let body = req.body;
    console.log(id);
    console.log(body.duenioActual);
    console.log(body.nuevoDuenio);

    Promise.all([
            actualizaPkToken(id, body.duenioActual),
            actualizaUsuario(id, body.nuevoDuenio, body.duenioActual)
        ])
        .then(respuestas => {
            console.log(respuestas);
            res.status(200).json({
                ok: true,
                // pktoken: respuestas[0],
                // usuario: respuestas[1]
            });
        });
})

let actualizaPkToken = (id, duenioActual) => {
    return new Promise((resolve, reject) => {
        pktoken.updateOne({ _id: id }, { $set: { duenioActual: Objectid(duenioActual), estatus: true } })
            .then((result) => {
                resolve(result)
            });
    });
}

let actualizaUsuario = (_idPkToken, nuevoDuenio, duenioActual) => {
    return new Promise((resolve, reject) => {
        Usuario.update({ _id: duenioActual }, { $pull: { pktokens: _idPkToken } })
            .then((result) => {
                console.log(result);

                resolve(result);
            });
        Promise.all([
                agregarPkTokenUsuario(_idPkToken, nuevoDuenio),
                removerPkTokenUsuario(_idPkToken, duenioActual)
            ])
            .then(respuestas => {
                resolve(respuestas)
            });
    });
}

let agregarPkTokenUsuario = (_idPkToken, nuevoDuenio) => {
    return new Promise((resolve, reject) => {
        Usuario.update({ _id: nuevoDuenio }, { $addToSet: { pktokens: _idPkToken } })
            .then((result) => {
                console.log(result);

                resolve(result);
            });
    });
}

let removerPkTokenUsuario = (_idPkToken, duenioActual) => {
    return new Promise((resolve, reject) => {
        Usuario.update({ _id: duenioActual }, { $pop: { pktokens: _idPkToken } })
            .then((result) => {
                console.log(result);

                resolve(result);
            });
    });
}

module.exports = app;